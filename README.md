*** Attention is a Skill Unlike All Others

When you learn a skill -- like typing -- the usualy strategy is to work yourself towards a state in which the physical work has been
turned over to some sort of autopilot so that the conscious mind is completely freed up to think at a higher level.
I imagine an ideal typist could just effortlessly look at each word, maybe sound it out, and move on the next. The
fingers would just do the right thing without ever even being noticed by the typist.

I like to define a "true musician" by whether or not she uses this strategy. Such a "true musician", is trying to reach a state where 
she only feels the music, and the body parts that produce the music are all onauto-pilot doing the right thing. She's never thinking
about where her fingers go. A "false musician" is moving towards a state defined by motions of the body, with the sound merely a secondary concern.

But learning the skill of focusing your attention completely, for extended periods of time, can't be accomplished by training an autopilot.
Paying attention is the ability of never letting the autopilot take over. It's doing the work yourself. In the world of typing,
it is *insisting* on being the false typist, the one that thinks about where each finger is moving, before it moves. Why would you do that?
It sounds painful. It's typing AND micromanaging yourself as you do it. It's trying to be perfect at something when there is no
need to be. Why should this interest me?

Because you're interested in the challenge and you want the side effect of being a better typist as a side effect.

Two types of attention

There are only two types of attention, which I'm calling pure and impure.

Pure attention is better known as "being in the zone". Your attention rate is so high that you and the thing you are concentrating on are the same thing in your mind.
You navigate through the problem space effortlessly because the essence of thing has merged with you. I don't experience it often, and I don't know how deep it
goes, but pure attention is a rare and elusive superpower. The way to get it is to be extremely interested in the thing you are putting your
attention on. Unfortunately, interest can't be manufactured easily. So, the road we almost always have to travel is that of impure attention.

Impure attention is the skill of unceasingly dragging yourself back to the thing you want to pay attention to. You practice when you medidate, I hope,
because that's my understanding of what it's all about. But you don't have to have it perfectly quiet to mediate, though that does help most
people. You can practice this art of impure attention by trying to do anything with your full attention.













