from blessed import Terminal
import sys
from time import sleep
from collections import deque
import random
from itertools import islice
import signal
import argparse
from string import digits, punctuation, printable, ascii_uppercase, ascii_lowercase

parser = argparse.ArgumentParser()
parser.add_argument('-g', '--goal', type=int, default=500, help='Number of characters to type without error.')
parser.add_argument('-c', '--show-count', action='store_true', help='Show countdown of characters left to reach goal.')
parser.add_argument('-t', '--show-history', action='store_true', help='Show a history of mistakes.')
parser.add_argument('-f', '--show-flash', action='store_true', help='Flash the characters red when a mistakes is made.')
parser.add_argument('-s', '--no-symbols', action='store_true', help='No symbol characters to type.')
parser.add_argument('-n', '--no-numbers', action='store_true', help='No number characters to type.')
parser.add_argument('-u', '--no-uppers', action='store_true', help='No uppercase characters to type.')
parser.add_argument('-l', '--no-lowers', action='store_true', help='No uppercase characters to type.')
parser.add_argument('-b', '--no-backticks', action='store_true', help='No backtick or tilde characters to type.')

args = parser.parse_args()

goal_reached = False
errors_offset = 3 if args.show_count else 1 # y value for start of display errors
clear_screen = False
countdown = args.goal  # the number of chars reamaining to be type to reach goal
visible = 5  # number of characters shown on the screen
if args.show_history:
    # 10 or 22 is max size of static columns and spaces
    width_required = 10 + visible + len(str(args.goal))
    height_required = 7
else:
    width_required = 22 + len(str(args.goal))
    height_required = 10

charset = []  # the set characters are chosen from randomly
charset += ascii_lowercase if not args.no_lowers else charset
charset += ascii_uppercase if not args.no_uppers else charset
charset += digits if not args.no_numbers else charset
charset += punctuation if not args.no_symbols else charset
if args.no_backticks:
    charset.remove('`')
if args.no_backticks:
    charset.remove('~')
chars = deque(list(random.choice(charset)  # the characters shown on the screen
                   for i in range(visible)), maxlen=visible)
char_history = deque([], maxlen=visible)  # history of characters shown
error_history = deque([])  # history of errors
display_color = 'normal'
exiting = False

def check_screen_size():
    if (term.height < height_required or
        term.width < width_required):
        sys.exit()

def on_resize(sig, action):
    global clear_screen
    clear_screen = True
    check_screen_size()

def clear():
    for x in range(0, term.width):
        for y in range(0, term.height):
            with term.location(x=x, y=y):
                print(' ')

def on_interrupt(sig, action):
    global exiting
    if exiting:  # Handle a second ^C
        sys.exit()
    exiting = True

    clear()
    min_countdown = countdown
    if True:  # Maybe we'll change this
        mistyped = {}
        for (pre, char, saved_countdown) in error_history:
            min_countdown = min(min_countdown, saved_countdown)
            mistyped.setdefault(pre[-1], []).append(char)
        with term.location(*top_left(0,2)):
            print('             GOAL: {0: >3}'.format(args.goal))
        with term.location(*top_left(0,3)):
            print('CURRENT COUNTDOWN: {0: >3}'.format(countdown))
        with term.location(*top_left(0,4)):
            print('MINIMUM COUNTDOWN:', min_countdown)
        with term.location(*top_left(0,5)):
            print('           ERRORS:', len(error_history))

        i = 5
        if mistyped:
            i = 7
            for key in sorted(mistyped, key=lambda k: len(mistyped[k]),
                              reverse=True):
                if i > term.height - 6:
                    break
                with term.location(*top_left(0,i)):
                    print('{} mistyped {} time(s)'.format(key, len(mistyped[key])))
                    i += 1

        i += 1
        for s in range(59,0,-1):
            with term.location(*top_left(0,i)):
                print("Exit in", s, "seconds.\n  ^C again to exit now.")
                sleep(1)
    sys.exit()

def top_left(x, y):
    return 2 + x, 1 + y

def print_errors():
    ERROR_DIGITS = 2  # 1 thru 99, then wrapping back to 0
    HISTORY_PREFIX = visible - 1
    HISTORY_SUFFIX_COLOR = '{t.green}'
    ERROR_COLOR = '{t.red}'
    COUNTDOWN_DIGITS = len(str(visible)) + 1  # +1 for the minus sign

    for i, (pre, char, saved_countdown) in enumerate(reversed(error_history),
                                                     start=1):
        _, y = top_left(0, i + errors_offset)
        if y >= term.height - 1:
            y += 1
            break
        pre = pre.strip()
        with term.location(*top_left(0,i+errors_offset)):
            num_len = len(error_history)
            num_col = '{{0: <{}}}'.format(ERROR_DIGITS)
            history_col = '{{1: >{}}}{{t.green}}{{2}}{{t.normal}}'.format(HISTORY_PREFIX)
            error_col = '{t.red}{3}{t.normal}'

            if saved_countdown <= 0:
                countdown_col = '{t.green}{4:<5}{t.normal}'
            else:
                countdown_col = '{t.normal}{4:<5}'

            msg = ' '.join((num_col, history_col, error_col, countdown_col))
            print(msg.format((num_len-i+1)%100, pre[:-1], pre[-1], char, saved_countdown, t=term))
    

# on resize, we clear the screen before we recenter things
signal.signal(signal.SIGWINCH, on_resize)

# on interupt we quit
signal.signal(signal.SIGINT, on_interrupt)

term = Terminal()
check_screen_size()

with term.fullscreen():
    with term.hidden_cursor():
        with term.cbreak():
            while True:
                out = []
                x = 0
                msg = '{{t.{}}}'.format(display_color)
                msg += '{0}'
                for c in chars:
                    with term.location(*top_left(x, 0)):
                        if x == 0:
                            msg = '{t.bold}' + msg
                        print(msg.format(c, t=term))
                        x += 1

                if args.show_count:
                    with term.location(*top_left(0,2)):
                        print("{0:<10}".format(countdown))

                # block until any single key is pressed.
                char = sys.stdin.read(1)
                if char not in printable or char in ('\r', '\n'):
                    char = '\uFFFD'  # Unicode REPLACEMENT CHARACTER
                # This is relevant when the window size changes
                if clear_screen:
                    for x in range(0, term.width):
                        for y in range(0, term.height):
                            with term.location(x=x, y=y):
                                print(' ')
                    clear_screen = False

                y = term.height // 2 + 1  # Move y down one line
                char_history.append(chars[0])
                if char != chars[0]:
                    if args.show_flash:
                        display_color = 'red'
                    error_history.append((''.join(char_history),
                                          char,
                                          countdown))
                    countdown = args.goal
                else:
                    countdown -= 1
                    if display_color != 'green':
                        display_color = 'normal'
                if display_color != 'red':
                    if countdown <= 0:
                        display_color = 'green'
                    else:
                        display_color = 'normal'

                chars.append(random.choice(charset))
                if args.show_history:
                    print_errors()
